//
// Created by Andrey Gostev on 09.02.2022.
//

#pragma once

#ifndef D3DX11_TEST_MYINPUT_H
#define D3DX11_TEST_MYINPUT_H

#include "common.h"

class MyInput : public InputListener {
public:
    bool KeyPressed(const KeyEvent &arg) override;

    bool MouseMove(const MouseEvent &arg) override;
};


#endif //D3DX11_TEST_MYINPUT_H
