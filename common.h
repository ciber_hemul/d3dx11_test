//
// Created by Andrey Gostev on 09.02.2022.
//

#pragma once

#ifndef D3DX11_TEST_COMMON_H
#define D3DX11_TEST_COMMON_H

#include "D3DX11_Framework.h"
#include "resource.h"

#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <d3d9.h>

#pragma comment(lib, "D3DCompiler.lib")

using namespace D3DX11_Framework;
using namespace DirectX;

#endif //D3DX11_TEST_COMMON_H
