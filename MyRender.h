//
// Created by Andrey Gostev on 09.02.2022.
//

#pragma once

#ifndef D3DX11_TEST_MYRENDER_H
#define D3DX11_TEST_MYRENDER_H

#include "common.h"

typedef struct SimpleVertex {
    XMFLOAT3 Pos;
    XMFLOAT4 Color;
} SIMPLE_VERTEX;

typedef struct ConstBuffer {
    XMMATRIX world;
    XMMATRIX view;
    XMMATRIX projection;
} CONSTANT_BUFFER;

class MyRender : public Render {
public:
    MyRender();

    bool Init(HWND wnd) override;

    bool Draw() override;

    void Close() override;

    void* operator new(size_t size);

    void operator delete(void* render);

private:
    ID3D11Buffer* m_vertexBuffer;
    ID3D11InputLayout* m_vertexLayout;
    ID3D11VertexShader* m_vertexShader;
    ID3D11PixelShader* m_pixelShader;
    ID3D11PixelShader* m_pixelSolidShader;

    vector<XMMATRIX> m_worlds;
    XMMATRIX m_view;
    XMMATRIX m_projection;

    ID3D11Buffer* m_constBuffer;
    ID3D11Buffer* m_indexBuffer;
};


#endif //D3DX11_TEST_MYRENDER_H
