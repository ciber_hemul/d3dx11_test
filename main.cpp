#include "MyRender.h"
#include "MyInput.h"

#ifdef _DEBUG
#define _ITERATOR_DEBUG_LEVEL 2
#endif

Framework framework;

LRESULT CALLBACK UserProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    switch (msg) {
        case WM_COMMAND:
            switch (wParam) {
                case IDM_EXIT:
                    framework.Close();
                    exit(0);
                default:
                    break;
            }
        default:
            return DefWindowProc(hWnd, msg, wParam, lParam);
    }
}

BOOL WINAPI closeConsole(DWORD msg) {
    if (msg == CTRL_CLOSE_EVENT) {
        framework.Close();
        Log::Instance()->~Log();
        return true;
    }
    return false;
}

int main()
{
    SetConsoleCtrlHandler(closeConsole, TRUE);

    auto *render = new MyRender();
    auto *input = new MyInput();

    DescWindow descWindow{100, 100, 640, 480, "New Caption"_cc, true};

    framework.SetRender(render);
    if( !framework.Init(descWindow, CS_HREDRAW | CS_VREDRAW, IDI_FRAME1, NULL, (HBRUSH) GetStockObject(BLACK_BRUSH), IDC_FRAME2, IDI_FRAME1, UserProc)) {
        framework.Close();

        return -1;
    }

    framework.AddInputListener(input);

    framework.Run();

    framework.Close();

    return 0;
}
