cbuffer constBuffer {
    matrix world;
    matrix view;
    matrix projection;
};

struct VSOut {
    float4 Pos : SV_POSITION;
    float4 Color : Color;
};

VSOut VSMain(float4 Pos : POSITION, float4 Color : Color) {
    VSOut output;
    output.Pos = mul(Pos, world);
    output.Pos = mul(output.Pos, view);
    output.Pos = mul(output.Pos, projection);
    output.Color = Color;
    return output;
}

float4 PSMain(VSOut input) : SV_Target {
    return input.Color;
}