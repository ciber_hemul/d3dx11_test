cmake_minimum_required(VERSION 3.21)
project(D3DX11_Test)

set(CMAKE_CXX_STANDARD 23)

include_directories(C:/Projects/D3DX11_Framework)
include_directories("C:/Program Files (x86)/Boost")

if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
    link_directories("C:/Projects/D3DX11_Framework/cmake-build-debug")
elseif(${CMAKE_BUILD_TYPE} STREQUAL Release)
    link_directories("C:/Projects/D3DX11_Framework/cmake-build-release")
endif()
link_libraries(D3DX11_Framework)

add_executable(D3DX11_Test main.cpp resource.h resource.rc MyRender.cpp MyRender.h common.h MyInput.cpp MyInput.h)

target_link_libraries(D3DX11_Test D3DX11_Framework)