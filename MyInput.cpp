//
// Created by Andrey Gostev on 09.02.2022.
//

#include "MyInput.h"

bool MyInput::KeyPressed(const KeyEvent &arg) {
    cout << "\"" << static_cast<char>(arg.wc) << "\" key pressed\n";
    return false;}

bool MyInput::MouseMove(const MouseEvent &arg) {
    cout << "Mouse on " << arg.pos.x << "-" << arg.pos.y << "\n";
    return false;
}
