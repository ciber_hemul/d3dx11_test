//
// Created by Andrey Gostev on 09.02.2022.
//

#include "MyRender.h"

bool MyRender::Init(HWND wnd) {
    ID3DBlob* vsBlob;

    if (FAILED(this->CompileShaderFromFile(L"shader.hlsl"_wc, "VSMain", "vs_5_0", &vsBlob, wnd))) {
        Log::Instance()->Error("Failed to compile shader file"_cc);
        return false;
    }

    if (FAILED(this->m_device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), NULL, &this->m_vertexShader))) {
        Log::Instance()->Error("Failed to create vertices shader"_cc);
        _release(vsBlob);
        return false;
    }

    D3D11_INPUT_ELEMENT_DESC ieDesc[] = {
            {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
            {"Color", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
    };

    if (FAILED(this->m_device->CreateInputLayout(ieDesc, ARRAYSIZE(ieDesc), vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), &this->m_vertexLayout))) {
        _release(vsBlob);
        return false;
    }
    _release(vsBlob);

    this->m_context->IASetInputLayout(this->m_vertexLayout);

    ID3DBlob* psBlob;

    if(FAILED(this->CompileShaderFromFile(L"shader.hlsl"_wc, "PSMain", "ps_5_0", &psBlob, wnd))) {
        Log::Instance()->Error("Failed to compile shader file"_cc);
        return false;
    }

    if (FAILED(this->m_device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), NULL, &this->m_pixelShader))) {
        Log::Instance()->Error("Failed to create pixel shader"_cc);
        _release(psBlob);
        return false;
    }

    SimpleVertex vertices[] = {
            {XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f)},
            {XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f)},
            {XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 1.0f, 1.0f, 1.0f)},
            {XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f)},
            {XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f)},
            {XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f)},
            {XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)},
            {XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f)}
    };

    D3D11_BUFFER_DESC vbDesc;

    ZeroMemory(&vbDesc, sizeof(vbDesc));

    vbDesc.ByteWidth = sizeof(SimpleVertex) * 8;
    vbDesc.Usage = D3D11_USAGE_DEFAULT;
    vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vbDesc.CPUAccessFlags = 0;
    vbDesc.MiscFlags = 0;

    D3D11_SUBRESOURCE_DATA subData;

    ZeroMemory(&subData, sizeof(subData));

    subData.pSysMem = vertices;

    if(FAILED(this->m_device->CreateBuffer(&vbDesc, &subData, &this->m_vertexBuffer))) {
        return false;
    }

    UINT size = sizeof(SimpleVertex);
    UINT offset = 0;
    this->m_context->IASetVertexBuffers(0, 1, &this->m_vertexBuffer, &size, &offset);

    WORD indices[] = {
        3, 1, 0,
        2, 1, 3,

        0, 5, 4,
        1, 5, 0,

        3, 4, 7,
        0, 4, 3,

        1, 6, 5,
        2, 6, 1,

        2, 7, 6,
        3, 7, 2,

        6, 4, 5,
        7, 4, 6,
    };

    D3D11_BUFFER_DESC ibDesc;

    ZeroMemory(&ibDesc, sizeof(ibDesc));

    ibDesc.Usage = D3D11_USAGE_DEFAULT;
    ibDesc.ByteWidth = sizeof(WORD) * 36;
    ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    ibDesc.CPUAccessFlags = 0;
    subData.pSysMem = indices;
    if(FAILED(this->m_device->CreateBuffer(&ibDesc, &subData, &this->m_indexBuffer)))
        return false;

    this->m_context->IASetIndexBuffer(this->m_indexBuffer, DXGI_FORMAT_R16_UINT, 0);

    this->m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    D3D11_BUFFER_DESC cbDesc;

    ZeroMemory(&cbDesc, sizeof(cbDesc));

    cbDesc.ByteWidth = sizeof(CONSTANT_BUFFER);
    cbDesc.Usage = D3D11_USAGE_DEFAULT;
    cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbDesc.CPUAccessFlags = 0;
    cbDesc.MiscFlags = 0;

    if (FAILED(this->m_device->CreateBuffer(&cbDesc, 0, &this->m_constBuffer))) {
        return false;
    }

    for (int i = 0; i < 2; i++) {
        this->m_worlds.push_back(XMMatrixIdentity());
    }

    XMVECTOR eye = {0.0f, 5.0f, -10.0f, 0.0f};
    XMVECTOR at = {0.0f, 1.0f, 0.0f, 0.0f};
    XMVECTOR up = {0.0f, 1.0f, 0.0f, 0.0f};

    this->m_view = XMMatrixLookAtLH(eye, at, up);

    RECT rc;

    GetWindowRect(wnd, &rc);

    this->m_projection = XMMatrixPerspectiveFovLH(XM_PIDIV2, (float)(rc.right - rc.left) / (float)(rc.bottom - rc.top), 0.01f, 100.0f);

    return true;
}

bool MyRender::Draw() {
    static float time = 0;
    static DWORD timeStart = 0;
    DWORD timeCurrent = GetTickCount();

    if (!timeStart) {
        timeStart = time;
    }

    time = (timeCurrent - timeStart) / 1000.0f;

    auto worldsCount = this->m_worlds.size();

    for (int i = 0; i < worldsCount; i++) {
        XMMATRIX scaling = XMMatrixScaling(0.3f + i, 0.3f + i, 0.3f + i);
        XMMATRIX rotationZ = XMMatrixRotationZ(-time * (i - 1.0f));
        XMMATRIX translation = XMMatrixTranslation(-4.0f + i, 0.0f + i, 0.0f + i);
        XMMATRIX rotationY = XMMatrixRotationY(-time * (2.0f + 2.0f * i));
        XMMATRIX rotationX = XMMatrixRotationX(-time * (2.0f + 10.0f * i));

        CONSTANT_BUFFER constBuffer;

        this->m_worlds[i] = scaling * rotationZ * translation * rotationY * rotationX;

        constBuffer.projection = XMMatrixTranspose(this->m_projection);
        constBuffer.view = XMMatrixTranspose(this->m_view);
        constBuffer.world = XMMatrixTranspose(this->m_worlds[i]);

        this->m_context->UpdateSubresource(this->m_constBuffer, 0, 0, &constBuffer, 0, 0);

        if (!i) {
            this->m_context->VSSetShader(this->m_vertexShader, NULL, 0);
            this->m_context->VSSetConstantBuffers(0, 1, &this->m_constBuffer);
            this->m_context->PSSetShader(this->m_pixelShader, NULL, 0);
        }

        this->m_context->DrawIndexed(36, 0, 0);
    }

    return true;
}

void MyRender::Close() {
    _release(this->m_context);
    _release(this->m_sChain);
    _release(this->m_tView);
    _release(this->m_device);
    _release(this->m_indexBuffer);
    _release(this->m_constBuffer);
}

MyRender::MyRender() {
    this->m_pixelShader = nullptr;
    this->m_vertexShader = nullptr;
    this->m_vertexBuffer = nullptr;
    this->m_vertexLayout = nullptr;
    this->m_constBuffer = nullptr;
    this->m_indexBuffer = nullptr;
}

void *MyRender::operator new(size_t size) {
    return _aligned_malloc(size, 16);
}

void MyRender::operator delete(void* render) {
    _aligned_free(render);
}
